/**
 * @file MyClass.cpp
 * @author David Alonso de la Torre (david.alonso@sidisel.com)
 * @brief Example class gotten from Mbed OS PwmOut to demonstrate
 * unit testing capabilities. Code is exactly the same but changing
 * the class name
 * @date 05/07/2021
 * 
 */
#include "MyClass.h"


#if DEVICE_PWMOUT

#include "platform/mbed_critical.h"
#include "platform/mbed_power_mgmt.h"
#include "platform/mbed_assert.h"

namespace example {

MyClass::MyClass(PinName pin) :
    _pin(pin),
    _deep_sleep_locked(false),
    _initialized(false),
    _duty_cycle(0),
    _period_us(0)
{
    MyClass::init();
}

MyClass::MyClass(const PinMap &pinmap) : _deep_sleep_locked(false)
{
    core_util_critical_section_enter();
    pwmout_init_direct(&_pwm, &pinmap);
    core_util_critical_section_exit();
}

MyClass::~MyClass()
{
    MyClass::deinit();
}

void MyClass::write(float value)
{
    core_util_critical_section_enter();
    pwmout_write(&_pwm, value);
    core_util_critical_section_exit();
}

float MyClass::read()
{
    core_util_critical_section_enter();
    float val = pwmout_read(&_pwm);
    core_util_critical_section_exit();
    return val;
}

void MyClass::period(float seconds)
{
    core_util_critical_section_enter();
    pwmout_period(&_pwm, seconds);
    core_util_critical_section_exit();
}

void MyClass::period_ms(int ms)
{
    core_util_critical_section_enter();
    pwmout_period_ms(&_pwm, ms);
    core_util_critical_section_exit();
}

void MyClass::period_us(int us)
{
    core_util_critical_section_enter();
    pwmout_period_us(&_pwm, us);
    core_util_critical_section_exit();
}

int MyClass::read_period_us()
{
    core_util_critical_section_enter();
    auto val = pwmout_read_period_us(&_pwm);
    core_util_critical_section_exit();
    return val;
}

void MyClass::pulsewidth(float seconds)
{
    core_util_critical_section_enter();
    pwmout_pulsewidth(&_pwm, seconds);
    core_util_critical_section_exit();
}

void MyClass::pulsewidth_ms(int ms)
{
    core_util_critical_section_enter();
    pwmout_pulsewidth_ms(&_pwm, ms);
    core_util_critical_section_exit();
}

void MyClass::pulsewidth_us(int us)
{
    core_util_critical_section_enter();
    pwmout_pulsewidth_us(&_pwm, us);
    core_util_critical_section_exit();
}

int MyClass::read_pulsewitdth_us()
{
    core_util_critical_section_enter();
    auto val = pwmout_read_pulsewidth_us(&_pwm);
    core_util_critical_section_exit();
    return val;
}

void MyClass::suspend()
{
    core_util_critical_section_enter();
    if (_initialized) {
        _duty_cycle = MyClass::read();
        _period_us = MyClass::read_period_us();
        MyClass::deinit();
    }
    core_util_critical_section_exit();
}

void MyClass::resume()
{
    core_util_critical_section_enter();
    if (!_initialized) {
        MyClass::init();
        MyClass::write(_duty_cycle);
        MyClass::period_us(_period_us);
    }
    core_util_critical_section_exit();
}

void MyClass::lock_deep_sleep()
{
    if (_deep_sleep_locked == false) {
        sleep_manager_lock_deep_sleep();
        _deep_sleep_locked = true;
    }
}

void MyClass::unlock_deep_sleep()
{
    if (_deep_sleep_locked == true) {
        sleep_manager_unlock_deep_sleep();
        _deep_sleep_locked = false;
    }
}

void MyClass::init()
{
    core_util_critical_section_enter();

    if (!_initialized) {
        pwmout_init(&_pwm, _pin);
        lock_deep_sleep();
        _initialized = true;
    }

    core_util_critical_section_exit();
}

void MyClass::deinit()
{
    core_util_critical_section_enter();

    if (_initialized) {
        pwmout_free(&_pwm);
        unlock_deep_sleep();
        _initialized = false;
    }

    core_util_critical_section_exit();
}

} // namespace mbed

#endif // #if DEVICE_PWMOUT
