# Mbed OS Unit Test example

This project is a demonstration about how to run
Unit Tests for Mbed OS 6 out-of-tree code.


## Description

The goal of this test is to run unit tests over a 
class which uses Mbed OS stubs/fakes but without 
using Mbed OS tree.

To do this, we have extracted a Mbed OS driver (PwmOut 
in this case) and changed its name. Then all its 
tests are run without modifying Mbed OS repo.

## Getting started

Usage of virtual environments is highly recommended,
you can also use the host mbed os tools. To run
a virtual environemnt using `pipenv` you can:

```bash
pipenv install --dev 
pipenv shell
```

Deploy Mbed OS

```bash
mbed deploy
```

## Run Unit Tests

1. Build enabling test capabilitied:

```bash
mkdir -p cmake_build
cmake -S . -B cmake_build -GNinja -DBUILD_TESTING=ON 
cmake --build cmake_build 
```

2. Run example tests:

```bash
(cd cmake_build && ctest -R example-*)
```
