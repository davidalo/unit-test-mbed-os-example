
# this is library definition
target_include_directories(local
    INTERFACE
        ${PROJECT_ROOT_DIR}/example
        ${PROJECT_ROOT_DIR}/example/include/
)

target_sources(local
    INTERFACE
        ${PROJECT_ROOT_DIR}/example
        ${PROJECT_ROOT_DIR}/example/source/MyClass.cpp
)